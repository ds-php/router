<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router;

use Ds\Router\Interfaces\AdaptorInterface;
use Ds\Router\Interfaces\RouteCollectionInterface;
use Ds\Router\Interfaces\RouterInterface;
use Ds\Router\RouteCollection;
use Ds\Router\Router;

/**
 * Class RouterTest
 * @package Tests\Ds\Router
 */
class RouterTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|AdaptorInterface
     */
    public $adaptor;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|RouteCollectionInterface
     */
    public $collection;

    /**
     * @var RouterInterface
     */
    public $router;


    /**
     *
     */
    public function setUp() : void
    {
        $this->adaptor = $this->getMockBuilder(AdaptorInterface::class)->getMock();
        $this->collection = $this->getMockBuilder(RouteCollectionInterface::class)->getMock();

        $this->router = new Router(
            $this->adaptor,
            $this->collection
        );
    }

    /**
     *
     */
    public function testGetRouterResponseFromPath()
    {
        $method = 'GET';
        $path = '/mypath';
        $actual = 'my-handler';

        $this->adaptor->expects($this->once())
            ->method('match')
            ->with($this->collection, $method, $path)
            ->willReturn($actual);

        $expected = $this->router->getRouterResponseFromPath($method, $path);
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testGetRouterResponse()
    {
        $method = 'GET';
        $path = '/path';
        $actual = 'my-handler';

        $this->adaptor->expects($this->once())
            ->method('match')
            ->with($this->collection, $method, $path)
            ->willReturn($actual);

        $expected = $this->router->getRouterResponseFromPath($method,$path);
        $this->assertEquals($expected, $actual);
    }


    /**
     *
     */
    public function testWithAdaptor()
    {
        $newAdaptor = $this->getMockBuilder(AdaptorInterface::class)->getMock();
        $newRouter = $this->router->withAdaptor($newAdaptor);
        $this->assertNotSame($this->adaptor, $newRouter->getAdaptor());
    }

    /**
     *
     */
    public function testGetAdaptor()
    {
        $expected = $this->adaptor;
        $actual = $this->router->getAdaptor();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testWithCollection()
    {
        $routes = $this->getMockBuilder(RouteCollectionInterface::class)->getMock();
        $this->router = $this->router->withCollection($routes);

        $this->assertSame(
            $routes,
            Helpers\Reflection::getProperty(Router::class, 'collection', $this->router)
        );
    }

    /**
     *
     */
    public function testGetCollection()
    {
        $expected = $this->collection;
        $actual = $this->router->getCollection();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testGetCollectionCached()
    {
        $this->adaptor->expects($this->once())
            ->method('isCached')
            ->willReturn(true);

        $this->adaptor->expects($this->once())
            ->method('getCachedRoutes')
            ->willReturn($this->collection);

        $collection = $this->router->getCollection();
        $this->assertSame($this->collection, $collection);
    }


    /**
     *
     */
    public function testMergeCollection()
    {
        $collection = new RouteCollection();

        $this->collection->expects($this->once())
            ->method('mergeCollection')
            ->with($this->equalTo($collection))
            ->willReturn($collection);

        $newCollection = $this->router->mergeCollection($collection);
        $this->assertNotSame($this->collection, $newCollection);
    }

    /**
     *
     */
    public function testIsCached()
    {
        $this->adaptor->expects($this->once())
            ->method('isCached')
            ->willReturn(true);
        $this->router->isCached();
    }
}
