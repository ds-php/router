<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router;

use Ds\Router\Exceptions\RouteException;
use Ds\Router\Route;
use Ds\Router\RouteCollection;

/**
 * Route Collection Tests.
 * @package Tests\Ds\Router
 */
class RouteCollectionTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var RouteCollection
     */
    public $collection;

    /**
     * Route Collection Set up.
     */
    public function setUp() : void
    {
        $this->collection = new RouteCollection();
    }

    /**
     *
     */
    public function testGroup()
    {
        $handler = 'my-handler';
        $routes = $this->collection;
        $routes->group('/path', function () use ($routes, $handler) {
            $routes->addRoute(['GET', 'POST'], '/foo', $handler, ['global']);
            $routes->group('/sub-dir', function () use ($routes, $handler) {
                $routes->addRoute(['GET'], '/sub-page', $handler, ['sub-dir']);
            });
        });
        foreach ($routes as $route) {
            $expected[] = $route;
        }
        $actual = Helpers\Reflection::getProperty(RouteCollection::class, 'collection', $routes);
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testAddRoute()
    {
        $handler = 'my-handler';
        $collection = new RouteCollection();
        $collection->addRoute(['GET', 'POST'], '/foo', $handler, ['global']);
        $actual = Helpers\Reflection::getProperty(RouteCollection::class, 'collection', $collection);
        $expected = [
            0 => new Route('GET', '/foo', $handler, ['global']),
            1 => new Route('POST', '/foo', $handler, ['global'])
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test that RouterException is thrown on duplicate route.
     */
    public function testIsUnique()
    {
        $this->expectException(RouteException::class);
        $handler = 'my-handler';
        $this->collection->addRoute('GET', '/foo', $handler, ['global']);
        $this->collection->addRoute('GET', '/foo', $handler, ['global']);
    }

    /**
     *
     */
    public function testCount()
    {
        $handler = 'my-handler';
        $collection = new RouteCollection();
        $collection->addRoute(['GET', 'POST'], '/foo', $handler, ['global']);
        $collection->addRoute(['GET', 'POST'], '/bar', $handler, ['global']);
        $expected = 4;
        $this->assertEquals($expected, $collection->count());
    }

    /**
     *
     */
    public function testKey()
    {
        $handler = 'my-handler';
        $collection = new RouteCollection();
        $collection->addRoute(['GET'], '/foo', $handler, ['global']);
        $collection->addRoute(['GET'], '/bar', $handler, ['global']);
        $expected = 0;
        $this->assertEquals($expected, $collection->key());
    }

    /**
     *
     */
    public function testNext()
    {
        $handler = 'my-handler';
        $collection = new RouteCollection();
        $collection->addRoute(['GET'], '/foo', $handler, ['global']);
        $collection->addRoute(['GET'], '/bar', $handler, ['global']);
        $expected = 1;
        $collection->next();
        $this->assertEquals($expected, $collection->key());
    }

    /**
     *
     */
    public function testCurrent()
    {
        $handler = 'my-handler';
        $collection = new RouteCollection();
        $collection->addRoute(['GET'], '/foo', $handler, ['global']);
        $collection->addRoute(['GET'], '/bar', $handler, ['global']);
        $expected = new Route('GET', '/foo', $handler, ['global']);
        $this->assertEquals($expected, $collection->current());
    }

    /**
     *
     */
    public function testValidTrue()
    {
        $handler = 'my-handler';
        $collection = new RouteCollection();
        $collection->addRoute(['GET'], '/foo', $handler, ['global']);
        $expected = true;
        $this->assertEquals($expected, $collection->valid());
    }

    /**
     *
     */
    public function testValidFalse()
    {
        $expected = false;
        $handler = 'my-handler';
        $collection = new RouteCollection();
        $collection->addRoute(['GET'], '/foo', $handler, ['global']);
        $collection->next();
        $this->assertEquals($expected, $collection->valid());
    }


    /**
     *
     */
    public function testRewind()
    {
        $handler = 'my-handler';
        $expected = new Route('GET', '/bar', $handler, ['global']);
        $collection = new RouteCollection();
        $collection->addRoute(['GET'], '/foo', $handler, ['global']);
        $collection->addRoute(['GET'], '/bar', $handler, ['global']);
        $collection->next();
        $this->assertEquals($expected, $collection->current());
        $expectedKey = 0;
        $collection->rewind();
        $this->assertEquals($expectedKey, $collection->key());
    }

    /**
     *
     */
    public function testMergeCollection()
    {
        $handler = 'handler::string';
        $names = ['routes','names'];
        $collection = new RouteCollection();
        $collection->addRoute('GET', '/path', $handler, $names);
        $collection->addRoute('GET', '/another', $handler, $names);
        $secondCollection = new RouteCollection();
        $secondCollection->addRoute('GET', '/path-2', $handler, $names);
        $secondCollection->addRoute('GET', '/another-2', $handler, $names);
        $combined = $collection->mergeCollection($secondCollection, false);
        $expectedRouteCount = 4;
        $this->assertEquals($expectedRouteCount, $combined->count());
    }

    /**
     *
     */
    public function testMergeCollectionDuplicate()
    {
        $this->expectException(RouteException::class);
        $handler = 'handler::string';
        $names = ['routes','names'];
        $collection = new RouteCollection();
        $collection->addRoute('GET', '/path', $handler, $names);
        $collection->addRoute('GET', '/another', $handler, $names);
        $secondCollection = new RouteCollection();
        $secondCollection->addRoute('GET', '/path', $handler, $names);
        $secondCollection->addRoute('GET', '/another-2', $handler, $names);
        $collection->mergeCollection($secondCollection, true);
    }

    /**
     *
     */
    public function testGetRoutes()
    {
        $method = 'GET';
        $pattern = '/path';
        $handler = 'myhandler';
        $names = ['names'];
        $expected = [new Route($method, $pattern, $handler, $names)];
        $this->collection->addRoute($method, $pattern, $handler, $names);
        $this->assertEquals($expected, $this->collection->getRoutes());
    }

    public function testFormatRoutePatternForRoute()
    {
        $rawPattern = '/foo/';
        $expectedPattern = '/' . \ltrim($rawPattern, '/');
        $this->assertEquals($expectedPattern, $this->collection->formatRoutePattern($rawPattern));
    }

    /**
     * Test that forward slashes are replaces with backslash
     */
    public function testFormatRoutePatternForGroup()
    {
        $groups = ['group'];
        $collection = $this->collection;
        $collection = Helpers\Reflection::setProperty(RouteCollection::class, '_isGrouped', $collection, true);
        $collection = Helpers\Reflection::setProperty(RouteCollection::class, 'group', $collection, $groups);
        $rawPattern = '/mypattern';
        $expectedPattern = '/' . \ltrim($rawPattern, '/');
        $expected = \implode('', $groups) . $expectedPattern;
        $this->assertEquals($expected, $collection->formatRoutePattern($rawPattern));
    }

    /**
     * Test that route names are merge with grouped routes.
     */
    public function testMergeGroupNames()
    {
        $expected = ['route-name','group-name'];
        $handler = 'myhandler';
        $routes = $this->collection;

        $routes->group('/path', function () use ($routes, $handler) {
            $routes->addRoute(['GET', 'POST'], '/foo', $handler, ['route-name']);
        }, ['group-name']);

        $route = $routes->current();
        $this->assertEquals($expected, $route->getNames());
    }

    /**
     * Test that the namespace is matched to the handler and returned.
     */
    public function testFindHandlerNamespace(){

        $routeCollection = new RouteCollection();
        $routeCollection->addNamespace('\Ds\Router');

        $handler = 'RouteCollection::addRoute';
        $expected = '\\Ds\\Router\\RouteCollection::addRoute';

        $reflectionMethod = new \ReflectionMethod(RouteCollection::class, '_findHandlerNamespace');
        $reflectionMethod->setAccessible(true);
        $actual = $reflectionMethod->invoke($routeCollection, $handler);

        $this->assertEquals($expected,$actual);
    }

    /**
     * Test that backlash is added to start of namespace on response.
     */
    public function testFindHandlerNamespaceNoBacklash(){

        $routeCollection = new RouteCollection();
        $routeCollection->addNamespace('Ds\Router');

        $handler = 'RouteCollection::addRoute';
        $expected = '\\Ds\\Router\\RouteCollection::addRoute';

        $reflectionMethod = new \ReflectionMethod(RouteCollection::class, '_findHandlerNamespace');
        $reflectionMethod->setAccessible(true);
        $actual = $reflectionMethod->invoke($routeCollection, $handler);

        $this->assertEquals($expected,$actual);
    }

}
