<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router\Adaptor;

use Ds\Router\Adaptor\AbstractAdaptor;
use Ds\Router\Interfaces\RouteCollectionInterface;

/**
 * Class AbstractAdaptorMock
 * @package Tests\Ds\Router\Adaptor
 */
class AbstractAdaptorMock extends AbstractAdaptor
{
    /**
     * @inheritdoc
     */
    public function match(RouteCollectionInterface $routes, $method, $requestTarget)
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function getCachedRoutes($context = '')
    {
        return $this->routes;
    }

    /**
     * @inheritdoc
     */
    public function isCached()
    {
        return false;
    }
}
