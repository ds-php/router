<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router\Adaptor;

use Ds\Router\Adaptor\FastRouteAdaptor;
use Ds\Router\Exceptions\RouterException;
use Ds\Router\Interfaces\SerializerInterface;
use Tests\Ds\Router\Helpers\Reflection;

/**
 * Class FastRouteAdaptorTest
 * @package Tests\Ds\Router\Adaptor
 */
class FastRouteAdaptorTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var FastRouteAdaptor
     */
    public $fastRoute;
    /**
     * @var array
     */
    public $options;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|SerializerInterface
     */
    public $serializer;

    /**
     *
     */
    public function setUp() : void
    {
        $this->options = [
            'cacheDisabled' => true,
            'errorHandlers' => [
                'default' => [
                    'handler' => 'errorController::method404',
                    'name' => ['error']
                ]
            ]
        ];

        $this->serializer = $this->getMockBuilder(SerializerInterface::class)->getMock();
        $this->fastRoute = new FastRouteAdaptor($this->serializer, $this->options);
    }

    /**
     *
     */
    public function testConstructMissingOptions()
    {
        $this->expectException(RouterException::class);
        return new FastRouteAdaptor($this->serializer, []);
    }

    /**
     *
     */
    public function testIsCachedWithCacheEnabled()
    {
        $expected = true;
        $adaptor = new FastRouteAdaptor($this->serializer, [
            'cacheDisabled' => $expected,
            'cacheFile' => __DIR__,
            'errorHandlers' => [
                'default' => [
                    'handler' => 'errorController::method404',
                    'name' => ['error']
                ]
            ]
        ]);

        $options = Reflection::getProperty(FastRouteAdaptor::class, 'options', $adaptor);
        $this->assertEquals($expected, $options['cacheDisabled']);
    }

    /**
     *
     */
    public function testMatch()
    {
        $fastRouteOptions = [
            'cacheDisabled' => true,
            'cacheFile' => __DIR__ ,
            'errorHandlers' => [
                'default' => [
                    'handler' => '\Site\Controllers\Error\ErrorController::error404',
                    'name' => ['error']
                ]
            ]
                    
        ];

        $router = new \Ds\Router\Router(
            new \Ds\Router\Adaptor\FastRouteAdaptor(
                new \Ds\Router\Serializer\SuperClosure(
                    new \SuperClosure\Serializer()
                ),
            $fastRouteOptions
            ), new \Ds\Router\RouteCollection()
        );
        
        
        //Routes collections can then be added to the router:
        $routeCollection = new \Ds\Router\RouteCollection();
        $routeCollection->addRoute('GET','/some-path','handler::string',['routeName']);
        $router = $router->withCollection($routeCollection);
        
        //Get response handler for the request.
        $response = $router->getRouterResponseFromPath('GET', '/some-path');
        $actual = $response->getHandler();
        $this->assertEquals('handler::string', $actual);
    }
}
