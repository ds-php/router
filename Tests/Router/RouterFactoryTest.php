<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router;

use Ds\Router\Adaptor\FastRouteAdaptor;
use Ds\Router\Interfaces\AdaptorInterface;
use Ds\Router\Interfaces\RouteCollectionInterface;
use Ds\Router\Interfaces\RouterInterface;
use Ds\Router\RouterFactory;

/**
 * Class RouterFactoryTest
 * @package Tests\Ds\Router
 */
class RouterFactoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var AdaptorInterface
     */
    public $adaptor;
    /**
     * @var RouteCollectionInterface
     */
    public $collection;

    /**
     *
     */
    public function setUp() : void
    {
        $this->adaptor = $this->getMockBuilder(AdaptorInterface::class)->getMock();
        $this->collection = $this->getMockBuilder(RouteCollectionInterface::class)->getMock();
    }

    /**
     * Test that create factory method is returning correct Router.
     */
    public function testCreateRouter()
    {
        $router = RouterFactory::createRouter($this->adaptor, $this->collection);
        $this->assertSame($this->adaptor, $router->getAdaptor());
        $this->assertSame($this->collection, $router->getCollection());
        $this->assertInstanceOf(RouterInterface::class, $router);
    }

    /**
     * Test that createFastRouter factory returns a correct instance of Router.
     */
    public function testCreateFastRouter()
    {
        $options = ['errorHandlers' => [
            'default' => [
                'handler' => '\Site\Controllers\Error\ErrorController::error404',
                'name' => ['error']
            ]
        ]];

        $router = RouterFactory::createFastRouter($options);
        $adaptor = $router->getAdaptor();
        $expectedOptions = $adaptor->getOptions();

        $this->assertInstanceOf(FastRouteAdaptor::class, $router->getAdaptor());
        $this->assertInstanceOf(RouteCollectionInterface::class, $router->getCollection());
        $this->assertEquals($expectedOptions, $options);
    }
}
