<?php

/**
 * PHP ROUTE FILE for Tests
 */

$collection = new \Ds\Router\RouteCollection();

$collection->addRoute('GET', '/path', function () use ($variable) {
    return $variable;
}, ['name']);

return $collection;
