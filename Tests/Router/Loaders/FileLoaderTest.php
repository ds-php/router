<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router\Loaders;

use Ds\Router\Interfaces\RouterInterface;
use Ds\Router\Interfaces\AdaptorInterface;
use Ds\Router\Interfaces\RouteCollectionInterface;
use Ds\Router\Exceptions\RouterException;
use Ds\Router\Loaders\FileLoader;
use Ds\Router\RouteCollection;

/**
 * Class FileLoaderTest
 * @package Tests\Ds\Router\Loaders
 */
class FileLoaderTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|RouterInterface
     */
    public $router;
    /**
     * @var FileLoader
     */
    public $loader;

    /**
     * @var array
     */
    public $vars;

    /**
     * @var string
     */
    public $file;

    /**
     * PHP Route File Loader setUp
     */
    public function setUp() : void
    {
        $options = [
            'vars' => [
                'variable' => 'my-var'
            ]
        ];

        $adaptor = $this->getMockBuilder(AdaptorInterface::class)->getMock();
        $routeCollection = $this->getMockBuilder(RouteCollectionInterface::class)->getMock();

      

        $this->file = __DIR__ . '/Files/FileLoaderRoutes.php';
        $this->router = $this->getMockBuilder(RouterInterface::class)
        ->setConstructorArgs([$adaptor, $routeCollection])
        ->getMock();
       
        $this->loader = new FileLoader($this->router, $options);
    }

    /**
     *
     */
    public function testLoadFileNoCacheNonInvalidFile()
    {
        $this->expectException(RouterException::class);
        $this->router->expects($this->once())
            ->method('isCached')
            ->willReturn(false);
        $this->loader->loadFile(__DIR__ . '/Files/Malformed.php');
    }

    /**
     *
     */
    public function testLoadFileNoCacheNoFile()
    {
        $this->expectException(RouterException::class);
        $this->router->expects($this->once())
            ->method('isCached')
            ->willReturn(false);
        $this->loader->loadFile(__DIR__ . '/RouteFileNone.php');
    }

    /**
     *
     */
    public function testLoadFileNoCacheWithVars()
    {
        $this->router->expects($this->once())
            ->method('isCached')
            ->willReturn(false);

        $routes = $this->loader->loadFile($this->file);
        $route = $routes->current();
        $handler = $route->getHandler();

        $expected = 'my-var';
        $actual = $handler();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testLoadFileWithCache()
    {
        $this->router->expects($this->once())
            ->method('isCached')
            ->willReturn(true);
        $this->router->expects($this->once())
            ->method('getCollection')
            ->willReturn(new RouteCollection());

        $collection = $this->loader->loadFile($this->file);
        $this->assertInstanceOf(RouteCollection::class, $collection);
    }

    /**
     *
     */
    public function testLoadFilesNoCacheNoFile()
    {
        $this->expectException(RouterException::class);

        $this->router->expects($this->any())
            ->method('isCached')
            ->willReturn(false);

        $this->router->expects($this->any())
            ->method('mergeCollection')
            ->willReturn($this->router);

        $this->loader->loadFiles([$this->file, __DIR__ . '/RouteFileNone.php']);
    }

    /**
     *
     */
    public function testLoadFilesNoCache()
    {
        $this->router->expects($this->any())
            ->method('isCached')
            ->willReturn(false);

        $this->router->expects($this->any())
            ->method('mergeCollection')
            ->willReturn($this->router);

        $this->loader->loadFiles([
            $this->file,
            __DIR__ . '/Files/FileLoaderRoutesAlt.php'
        ]);
        $this->assertEquals(false, $this->router->isCached());
    }

    /**
     *
     */
    public function testLoadFilesCache()
    {
        $this->router->expects($this->any())
            ->method('isCached')
            ->willReturn(true);
        $router = $this->loader->loadFiles([
            $this->file,
            __DIR__ . '/Files/FileLoaderRoutesAlt.php'
        ]);
        $this->assertSame($this->router, $router);
    }
}
