<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Tests\Ds\Router\Dispatcher;

use Ds\Router\Dispatcher\Dispatcher;
use Tests\Ds\Router\Helpers\Reflection;

/**
 * Class DisparcherTest
 * @package Tests\Ds\Router\Dispatcher
 */
class DisparcherTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Dispatcher
     */
    public $dispatcher;

    public function setUp() : void
    {
        $options = [];
        $this->dispatcher = new Dispatcher($options);
    }

    public function testWithNamespace()
    {
        $ns = 'myNamespace';
        $dispatcher = $this->dispatcher->withNamespace($ns);
        $addedNamespaces = Reflection::getProperty(Dispatcher::class, 'namespaces', $dispatcher);
        $this->assertEquals([$ns => $ns], $addedNamespaces);
    }

    public function testWithNamespaces()
    {
        $namespaces = [
            '\my\namespace','another','one\more'
        ];

        $expected = [
            $namespaces[0] => $namespaces[0],
            $namespaces[1] => $namespaces[1],
            $namespaces[2] => $namespaces[2]
        ];

        $dispatcher = $this->dispatcher->withNamespaces($namespaces);
        $addedNamespaces = Reflection::getProperty(Dispatcher::class, 'namespaces', $dispatcher);
        $this->assertEquals($expected, $addedNamespaces);
    }
}
