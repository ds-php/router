# Ds\Router

PHP Router

Adaptor Based Routing Component.

Includes the following Adaptors:
 * Fast Route (with cached routes for String|Closure)
 * Symfony Routing


## Creating the Router

```
    $router = new Ds\Router\Router(
        Ds\Router\Interfaces\AdaptorInterface,
        Ds\Router\Interfaces\RouteCollectionInterface
    )

    //Fast Router with Serialized Routes.
    $router = new Router(
        new FastRouteAdaptor(
            new SuperClosure(
                new Serializer()
            ),
            (array) $options
        ),
        new RouteCollection()
    );

    //Use the Factory methods for easy access
    $router = \Ds\Router\RouterFactory::createFastRouter($options);
```

### Router Adaptors

Adaptors must implement 'Ds\Router\Interfaces\AdaptorInterface

Provided Adaptors:
 * FastRoute - Ds\Adaptors\FastRouteAdaptor
 * Symfony - Ds\Adaptors\SymfonyAdaptor

```
    Ds\Router\Adaptor\FastRouteAdaptor(
        SerializerInterface $serializer,
        array $options = []
    );


    $fastRoute = new FastRouteAdaptor(
         new SuperClosure(
            new Serializer()
         ),
         [
             'cacheDisabled' => FALSE,
             'cacheFile' => __DIR__ . '/routes.cache',
             'cacheExpires' => 60 * 60 * 24 * 7,
             'errorHandlers' => [
                 'default' => [
                     'handler' => '\Site\Controllers\Error\ErrorController::error404',
                     'name' => ['error']
                 ]
             ]
         ]
    )

$router = $router->withAdaptor($fastRoute)

```


### Route Collections

Routes Collections must implement \Ds\Router\Interfaces\RouteCollectionInterface.

```
$routeCollection = new \Ds\Router\RouteCollection();

$routeCollection->group('/sub-dir', function() use ($routeCollection){

    $routeCollection->addRoute(
            (string)$httpMethod,
            (string)$path,
            (string)$handler,
            (array)$names
    );
});

$router = $router->withCollection($routeCollection)

```

### Loaders

Collection Loaders must implement \Ds\Router\Interfaces\LoaderInterface.
File loader:

```
$fileLoader = new Ds\Router\Loaders\FileLoader($router, [
    'vars' => [
        'varname' => $variable,
    ]
]);
```

Routes.php

```
<?php

/**
 * RouteFile.php
 */

$collection = new \Ds\Router\RouteCollection();

$collection->addRoute('GET', '/path', function () use ($varname) {
    return $varname;
}, ['name']);

return $collection;

```
Provide routes:

```
$loader->loadFile(__DIR__ . '/RouteFile.php');

```

## Using the router.

Once the adaptor and routes have been added Routes can be matched to the PSR7 request and a Ds\Router\RouterResponse returned


### Dispatching Routes.

Use Router::getRouterResponse() to return the RouterResponse for that request.

```
$res = $router->getRouterResponseFromPath($method,$path)

```

### Dispatching Handlers.

Handlers must be resolved from the in order to get the response from the controller/closure. This is useful when dealing with expensive calls or to areas of the site than might not be accessible before/after
middleware e.g. Authentication. Trying RouterResponse::getResponse() without resolving will cause an Exception to be thrown.

#### Example

```
$fastRouteOptions = [
    'cacheDisabled' => false,
    'cacheFile' => __DIR__ . '/routes.cache',
    'cacheExpires' => 60 * 60 * 24 * 7,
    'errorHandlers' => [
        'default' => [
            'handler' => '\Site\Controllers\Error\ErrorController::error404',
            'name' => ['error']
        ]
    ]
];
$router = new Ds\Router\Router(
    new Ds\Router\Adaptor\FastRouteAdaptor(
        new Ds\Router\Serializer\SuperClosure(
            new SuperClosure\Serializer()
        ),
        $fastRouteOptions
    ), new Ds\Router\RouteCollection()
);

//The above is the same as using the factory helper:
$router = \Ds\Router\RouterFactory::createFastRouter($fastRouteOptions);


//Routes collections can then be added to the router:
$routeCollection = new \Ds\Router\RouteCollection();
$routeCollection->addRoute('GET','/some-path','handler::string',['routeName']);
$router->withCollection($routeCollection);

$app = new \StdClass();

//Or routes can be added via seperate files via the FileLoader
//Variables can be made accessible via 'vars' option.
$fileLoader = new Ds\Router\Loaders\FileLoader($router, [
    'vars' => [
        'app' => $app,
    ]
]);

//load routes from Routes/routes.php and Routes/routes2.php
$router = $fileLoader->loadFiles([
    __DIR__ . '/Routes/routes.php',
    __DIR__ . '/Routes/routes2.php'
]);

//Or routes can be added via yaml via the YamlLoader
$yamlLoader = new \Ds\Router\Loaders\YamlLoader($router);
$router = $yamlLoader->loadFiles([__DIR__ . '/Routes/routes.yml']);


//Get response handler for the request.
$response = $router->getRouterResponseFromPath('GET', '/newPath/new');

//Load Dispatcher to convert handler intro response.
$dispatcher = new \Ds\Router\Dispatcher\Dispatcher([
    'whitelist' => ['ControllerInterface'],
    'blacklist' => ['ControllerInterface2'],
    'autoWire' => true
]);

//Dispatch the request with params to pass to controller.
$dispatched = $dispatcher->dispatch(
    $serverRequest,
    $response->getHandler(),
        [
        'container' => '-my-container-class'
    ]
);

var_dump($dispatched->getBody()->getContents());

```