<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Loaders;

use Ds\Router\Exceptions\RouterException;
use Ds\Router\Interfaces\RouteCollectionInterface;
use Ds\Router\Interfaces\RouterInterface;

/**
 * Class FileLoader
 *
 * @package Ds\Router\Loaders
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class FileLoader extends AbstractLoader
{
    /**
     * File Loader Default settings
     *
     * @var array
     */
    public static $defaults = [
        'vars' => []
    ];

    /**
     * File Loader Options
     *
     * @var array
     */
    protected $options;

    /**
     * FileLoader constructor.
     *
     * @param RouterInterface $router Routing Component
     * @param array $options File Loader Options.
     */
    public function __construct(RouterInterface $router, array $options = [])
    {
        $this->options = \array_replace_recursive(FileLoader::$defaults, $options);
        $this->router = $router;
    }

    /**
     * @inheritdoc
     */
    public function loadFiles(array $files)
    {
        

        $this->signature = $this->signature ?? $this->createCacheSignature($files);

        if (!$this->router->isCached($this->signature)) {
            foreach ((array)$files as $filename) {
                $collection = $this->loadFile($filename);
                $this->router = $this->router->mergeCollection($collection);
            }
        }
        return $this->router;
    }

    /**
     * @inheritdoc
     */
    public function loadFile(string $file)
    {
        $this->signature = $this->signature ?? $this->createCacheSignature([$file]);

        if (!$this->router->isCached($this->signature)) {
            foreach ((array)$this->options['vars'] as $key => $item) {
                if (\is_string($key)) {
                    ${$key} = $item;
                }
            }

            if (!\file_exists($file)) {
                throw new RouterException("Could not locate route file.'{$file}'");
            }

            $routeData = require $file;

            if ($routeData instanceof RouteCollectionInterface === false) {
                throw new RouterException("'{$file}' must return an instance of Rs\\Router\\RouteCollectionInterface");
            }

            return $routeData;
        }
        return $this->router->getCollection();
    }
}
