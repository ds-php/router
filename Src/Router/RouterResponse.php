<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router;

use Ds\Router\Interfaces\RouterResponseInterface;

/**
 * RouterResponse Class.
 *
 * Contains information about matched Route.
 *
 * @package Ds\Router
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class RouterResponse implements RouterResponseInterface
{
    /**
     * @var int Response Status Code.
     */
    protected $statusCode;

    /**
     * @var string|\Closure Route Handler.
     */
    protected $handler;

    /**
     * @var array Route Query Params.
     */
    protected $vars = [];

    /**
     * @var array Route Names
     */
    protected $names = [];

    /**
     * Router Response constructor.
     *
     * @param int $statusCode
     * @param string|\Closure $handler
     * @param array $names
     * @param array $vars
     */
    public function __construct(
        $statusCode,
        $handler,
        array $names = [],
        array $vars = []
    )
    {
        $this->statusCode = $statusCode;
        $this->handler = $handler;
        $this->names = $names;
        $this->vars = $vars;
    }

    /**
     * @inheritdoc
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @inheritdoc
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @inheritdoc
     */
    public function getVars()
    {
        return $this->vars;
    }

    /**
     * @inheritdoc
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param string $handler
     * @return RouterResponse
     */
    public function withHandler(string $handler){
        $new = clone $this;
        $new->handler = $handler;
        return $new;
    }

    /**
     * @param array $vars
     * @return RouterResponse
     */
    public function withVars(array $vars){
        $new = clone $this;
        $new->vars = $vars;
        return $new;
    }

    /**
     * @param int $code
     * @return RouterResponse
     */
    public function withStatusCode(int $code){
        $new = clone $this;
        $new->statusCode = $code;
        return $new;
    }

    /**
     * @param array $names
     * @return RouterResponse
     */
    public function withNames(array $names){
        $new = clone $this;
        $new->names = $names;
        return $new;
    }
}
