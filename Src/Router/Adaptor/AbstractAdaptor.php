<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Adaptor;

use Ds\Router\Interfaces\AdaptorInterface;
use Ds\Router\Interfaces\RouteCollectionInterface;
use Ds\Router\Interfaces\SerializerInterface;

/**
 * Class AbstractAdaptor
 *
 * @package Ds\Router\Adaptor
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
abstract class AbstractAdaptor implements AdaptorInterface
{
    /**
     * @var SerializerInterface
     */
    public $serializer;
    /**
     * @var array
     */
    protected $options = [];

    /**
     * Adaptor constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @inheritdoc
     */
    public function getOption($key)
    {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function withOptions(array $options = [])
    {
        $this->options = $options;
    }

    /**
     * @inheritdoc
     */
    abstract public function match(RouteCollectionInterface $routes, $method, $requestTarget);

    /**
     * @inheritdoc
     */
    abstract public function getCachedRoutes($context = '');

    /**
     * @inheritdoc
     */
    abstract public function isCached();
}
