<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Adaptor;

use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Ds\Router\Exceptions\AdaptorException;
use Ds\Router\Exceptions\RouterException;
use Ds\Router\Interfaces\RouteCollectionInterface;
use Ds\Router\Interfaces\RouteInterface;
use Ds\Router\Interfaces\SerializerInterface;
use Ds\Router\RouteCollection;
use Ds\Router\RouterResponse;

/**
 * Class FastRouteAdaptor
 *
 * @package Ds\Router\Adaptor
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class FastRouteAdaptor extends AbstractAdaptor
{
    /**
     * FastRouteAdaptor constructor.
     *
     * @param SerializerInterface $serializer Serializer for Cached Routes.
     * @param array               $options    Fast Route options.
     *
     * @throws RouterException
     */
    public function __construct(SerializerInterface $serializer, array $options = [])
    {
        parent::__construct($serializer);
        $this->options = $options;

        if (!isset($this->options['errorHandlers']['default'])) {
            throw new RouterException('Error Handlers must be specified');
        }

        $this->_checkCacheExpires();
    }

    /**
     * Check that cache file hasn't expired.
     * @return bool
     * @throws RouterException
     */
    protected function _checkCacheExpires()
    {
        if (isset($this->options['cacheExpires'], $this->options['cacheFile'])
            && $this->isCached()
        ) {
            $timeCacheCreated = \filemtime($this->options['cacheFile']);
            $timeNow = \time();
            $timeExpires = $timeCacheCreated + $this->options['cacheExpires'];
            if ($timeExpires - $timeNow < 0) {
                $removed = \unlink($this->options['cacheFile']);
                if (!$removed) {
                    throw new RouterException('Unable to delete file: ' . $this->options['cacheFile']);
                }
            }
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function isCached($signature = null)
    {

        $cacheDir = $this->options['cacheFile'] ?? __DIR__;
        $signatureFile = $cacheDir.'.signature';

        $signatureMatch = false;

        if ($signature !== null && file_exists($signatureFile)) { //found signature cache file.
            $oldSignature = \json_decode(file_get_contents($signatureFile));
            if ((string)$oldSignature !== (string)$signature) { //signature matches
                unlink($cacheDir);
                unlink($signatureFile);
            }
        }

        $disabledCache = isset($this->options['cacheDisabled']) ? (bool)$this->options['cacheDisabled'] : true;

        if (!$disabledCache && $signatureMatch === false){
            file_put_contents($signatureFile, \json_encode($signature));
        }

        if ($disabledCache === false && $signatureMatch) {
            return \file_exists($cacheDir) ? true : false;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function match(RouteCollectionInterface $routes, $method, $requestTarget)
    {
        $fastRouteResponse = [
            'statusCode' => 500,
            'allowedMethods' => [],
            'vars' => []
        ];

        $foundRoute = $this->_getCachedDispatcher($routes)->dispatch($method, $requestTarget);

        switch ($foundRoute[0]) {
            case Dispatcher::FOUND:
                $fastRouteResponse['statusCode'] = 200;
                $fastRouteResponse['vars'] = $foundRoute[2];
                $fastRouteResponse['handler'] = $foundRoute[1];
                break;
            case Dispatcher::NOT_FOUND:
                $fastRouteResponse['statusCode'] = 404;
                $fastRouteResponse['handler'] = $this->_findOptionsHandler(404);
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                $fastRouteResponse['statusCode'] = 405;
                $fastRouteResponse['handler'] = $this->_findOptionsHandler(405);
                break;
            default:
                $fastRouteResponse['handler'] = $this->_findOptionsHandler('default');
        }
        return $this->_createRouterResponse($fastRouteResponse);
    }

    /**
     * Get Fast Route Cached Dispatcher.
     *
     * @param RouteCollectionInterface $routes
     * @param array $options
     * @return Dispatcher
     */
    protected function _getCachedDispatcher(RouteCollectionInterface $routes, array $options = array())
    {
        $options += $this->options;
        $this->options = $options;

        return \FastRoute\cachedDispatcher(
            function (RouteCollector $fastRouteCollector) use ($routes) {
                /**
                 * @var $route RouteInterface
                 */
                foreach ($routes as $route) {
                    $fastRouteCollector->addRoute(
                        $route->getMethod(),
                        $route->getPattern(),
                        $this->_createFastRouteHandler($route)
                    );
                }
            },
            $this->options
        );
    }

    /**
     * @param RouteInterface $route
     * @return array
     */
    protected function _createFastRouteHandler(RouteInterface $route)
    {
        $handlerOut = [
            'type' => $route->getHandlerType(),
            'name' => $route->getNames()
        ];

        if ($route->getHandlerType() === 'object') {
            $handlerOut['content'] = $this->serializer->serialize($route->getHandler());
        } else {
            $handlerOut['content'] = $route->getHandler();
        }

        return $handlerOut;
    }

    /**
     * Return handler if it exists.
     * @param $code
     * @return mixed
     */
    protected function _findOptionsHandler($code)
    {
        $handlerArgs = $this->options['errorHandlers']['default'];

        if (isset($this->options['errorHandlers'][$code])) {
            $handlerArgs = $this->options['errorHandlers'][$code];
        }

        return [
            'type' => 'string',
            'content' => $handlerArgs['handler'],
            'name' => $handlerArgs['name']
        ];
    }

    /**
     * Unserialize any cached content and return RouterResponse.
     *
     * @param $dispatchResponse
     * @return RouterResponse
     */
    protected function _createRouterResponse($dispatchResponse)
    {
        if ($dispatchResponse['handler']['type'] === 'object') {
            $dispatchResponse['content'] = $this->serializer->unserialize($dispatchResponse['handler']['content']);
        } else {
            $dispatchResponse['content'] = $dispatchResponse['handler']['content'];
        }

        return new RouterResponse(
            $dispatchResponse['statusCode'],
            $dispatchResponse['content'],
            $dispatchResponse['handler']['name'],
            $dispatchResponse['vars']
        );
    }

    /**
     * @inheritdoc
     *
     * @throws \Rs\Router\Exceptions\UniqueRouteException
     */
    public function getCachedRoutes($context = '')
    {
        $collection = new RouteCollection();

        if ($this->isCached()) {

            $cacheFile = $this->getOption('cacheFile');

            if (!\file_exists($cacheFile)) {
                throw new AdaptorException('Cache file: ' . $cacheFile . ' not found');
            }

            $routes = (array)require $cacheFile;

            foreach ($routes[0] as $routeMethod => $routeData) {
                foreach ($routeData as $routePattern => $routeContent){
                    $routePath = \rtrim($context, '/') . DIRECTORY_SEPARATOR . \ltrim($routePattern, '/');
                    $collection->addRoute($routeMethod, $routePath, $routeContent['content'], $routeContent['name']);
                }
            }
        }
        return $collection;
    }
}
