<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Dispatcher;

use Ds\Router\Exceptions\DispatchException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response;
use Zend\Diactoros\Stream;

/**
 * Class Dispatcher
 *
 * @package Ds\Router\Dispatcher
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class Dispatcher
{
    /**
     * Dispatcher default options.
     *
     * @var array
     */
    protected static $defaults = [
        'stringHandlerPattern' => '::',
        'autoWire' => true
    ];
    /**
     * @var array
     */
    private $options;
    /**
     * @var array
     */
    private $providedArguments;
    /**
     * @var array
     */
    private $constructParams;

    /**
     * Handler Class Namespaces.
     *
     * @var array.
     */
    private $namespaces = [];

    /**
     * Dispatcher constructor.
     *
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        $options = \array_replace_recursive(self::$defaults, $options);
        $this->options = $options;
    }

    /**
     * Get response from handler string.
     *
     * @param $request
     * @param string|\Closure $handler
     * @param array $constructor
     * @return ResponseInterface
     * @throws \Exception
     */
    public function dispatch(ServerRequestInterface $request, $handler, array $constructor = array())
    {
        $handlerType = \gettype($handler);
        $response = null;

        switch ($handlerType) {
            case 'string':

                $params = \explode($this->options['stringHandlerPattern'], $handler);
                if (!isset($params[1])){
                    $response = $handler;
                    break;
                }

                $resolvedHandler = $this->_processStringHandler($handler);
                $this->_getParams($resolvedHandler['controller'], $constructor);
                $response = $this->_callController($request, $resolvedHandler['controller'], $resolvedHandler['method']);
                break;
            case 'object':
                if (\is_callable($handler)) {
                    $response = $handler($request);
                }
                break;
            default:
                throw new DispatchException(
                    "Handler type: {$handlerType} is not valid."
                );
        }

        if (!$response instanceof ResponseInterface) {
            $response = $this->createNewResponse($response);
        }
        return $response;
    }

    /***
     * Get Controller/Method from handler string.
     *
     * @param $handler
     * @return mixed
     * @throws DispatchException
     */
    protected function _processStringHandler($handler)
    {
        $controllerMethod = \explode(
            $this->options['stringHandlerPattern'], $handler
        );

        try {
            $resolvedHandler['controller'] = $this->_findClass($controllerMethod[0]);
        } catch (\Exception $e) {
            throw new DispatchException($e->getMessage());
        }

        $resolvedHandler['method'] = $controllerMethod[1] ?? null;

        return $resolvedHandler;
    }

    /**
     * Find class from namespace.
     *
     * @param string $class
     * @return string
     * @throws DispatchException
     */
    protected function _findClass($class)
    {
        if (\class_exists($class)) {
            return $class;
        }

        foreach ($this->namespaces as $namespace) {
            if (\class_exists($namespace . $class)) {
                return $namespace . $class;
            }
        }

        throw new DispatchException("Controller : {$class} does not exist");
    }

    /**
     * Find class construct parameters from provided arguments.
     *
     * @param string $controllerName
     * @param array $args
     * @return array
     * @throws DispatchException
     */
    protected function _getParams(string $controllerName, array $args)
    {
        if ($this->options['autoWire'] === false) {
            $this->providedArguments = $args;
            return $this->providedArguments;
        }

        $this->providedArguments = [];
        $this->constructParams = $this->_getConstructParams($controllerName);

        try {
            $this->_getParamsFromVariableName($args);
        } catch (\Exception $e) {
            $this->_getParamsFromTypeHint($args);
        }

        if (\count($this->providedArguments) - \count($this->constructParams) > 0) {
            throw new DispatchException('Missing parameters');
        }

        \ksort($this->providedArguments);
        return $this->providedArguments;
    }

    /**
     * Reflect Controller construct and get parameters.
     *
     * @param $controllerName
     * @internal
     *
     * @return \ReflectionParameter[]
     */
    protected function _getConstructParams($controllerName)
    {
        $controllerReflection = new \ReflectionClass($controllerName);
        $constructor = $controllerReflection->getConstructor();

        if (null === $constructor) {
            return [];
        }

        return $constructor->getParameters();
    }

    /**
     * Find controller constructor params based on $arg[index]
     *
     * @param $args
     * @throws DispatchException
     */
    protected function _getParamsFromVariableName($args)
    {
        foreach ($this->constructParams as $index => $param) {
            $parameterVarName = $param->getName();
            $constructorParamIndex = (int)$param->getPosition();

            if (isset($args[$parameterVarName])) {
                $this->providedArguments[$constructorParamIndex] = $args[$parameterVarName];
            }
        }
        if (\count($this->constructParams) - \count($this->providedArguments) > 0) {
            throw new DispatchException('Missing parameters');
        }
    }

    /**
     * Find missing parameters based on Class name or type hint.
     *
     * @param $args
     */
    protected function _getParamsFromTypeHint($args)
    {
        $processedArgs = $this->_processConstructArgs($args);

        foreach ($this->constructParams as $index => $param) {

            /**
             * @var \ReflectionClass $constructorParamClass ;
             */
            $constructorParamClass = $param->getClass();
            $constructorParamIndex = (int)$param->getPosition();

            if (isset($this->providedArguments[$constructorParamIndex])) {
                continue;
            }

            if ($constructorParamClass) {
                $constructParamTypeHint = $constructorParamClass->getName();
                foreach ((array)$processedArgs['object'] as $useParam) {
                    $key = \array_search($constructParamTypeHint, $useParam['interfaces'], true);
                    if ($key !== false) {
                        $this->providedArguments[$constructorParamIndex] = $useParam['value'];
                    }
                }
            }
        }
    }

    /**
     * Process provided arguments.
     *
     * @param array $args
     * @return array
     */
    protected function _processConstructArgs(array $args = [])
    {
        $arguments = [];
        foreach ($args as $index => $param) {
            switch (\gettype($param)) {
                case 'object':
                    $arguments['object'][] = [
                        'class' => \get_class($param),
                        'interfaces' => \class_implements($param),
                        'value' => $param
                    ];
                    break;
            }
        }
        return $arguments;
    }

    /**
     * Call controller with provided request and return Response.
     *
     * @param ServerRequestInterface $request     Server Request
     * @param string                 $controller  Controller class.
     * @param string                 $method      Controller method.
     *
     * @return ResponseInterface
     * @throws DispatchException
     */
    protected function _callController(ServerRequestInterface $request, string $controller, string $method)
    {

        if (!class_exists($controller)){
            throw new DispatchException('Unable to locate controller: '.$controller);
        }

        $controllerObj = new $controller(...$this->providedArguments);

        try {
            $this->_checkWhiteList($controller, $this->options);
            $this->_checkBlackList($controller, $this->options);
        } catch (\Exception $e) {
            unset($controllerObj);
            throw new DispatchException($e->getMessage());
        }

        if (!method_exists($controllerObj,$method)){
            throw new DispatchException('Unable to locate method: '.$controller.'::'.$method);
        }

        return $controllerObj->{$method}($request);
    }

    /**
     * Checks controller instance against whitelist.
     *
     * @param $controller
     * @param $options
     * @throws DispatchException
     */
    protected function _checkWhiteList($controller, $options)
    {
        if (isset($options['blacklist'])) {
            if (!$this->_checkInList($controller, $options['blacklist'])) {
                throw new DispatchException("Controller: {$controller}, not part of allowed whitelist.");
            }
        }
    }

    /**
     * Internal function for checking controller instance against class list.
     *
     * @param $controller
     * @param array $classList
     * @internal
     * @return bool
     */
    protected function _checkInList($controller, array $classList)
    {
        foreach ((array)$classList as $classes) {
            if ($controller instanceof $classes) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks controller instance against blacklist.
     *
     * @param $controller
     * @param $options
     * @throws DispatchException
     */
    protected function _checkBlackList($controller, $options)
    {
        if (isset($options['blacklist'])) {
            if ($this->_checkInList($controller, $options['blacklist'])) {
                throw new DispatchException("Controller: {$controller}, found on blacklist.");
            }
        }
    }

    /**
     * Create a new PSR-7 Response.
     *
     * @param $controllerResponse
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function createNewResponse($controllerResponse)
    {
        $response = new Response();
        $body = new Stream("php://memory", "wb+");
        $body->write($controllerResponse);
        $body->rewind();
        return $response->withBody($body);
    }

    /**
     * With Class Namespace.
     *
     * @param $namespace
     * @return Dispatcher
     */
    public function withNamespace($namespace)
    {
        $new = clone $this;
        $new->namespaces[$namespace] = $namespace;
        return $new;
    }

    /**
     * With class Namespaces.
     *
     * @param array $namespaces
     * @return Dispatcher
     */
    public function withNamespaces(array $namespaces)
    {
        $new = clone $this;
        foreach ($namespaces as $ns) {
            $new->namespaces[$ns] = $ns;
        }
        return $new;
    }
}
