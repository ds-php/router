<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Serializer;

use Ds\Router\Exceptions\RouteException;
use Ds\Router\Interfaces\SerializerInterface;
use SuperClosure\Serializer as SuperClosureSerializer;

/**
 * Class SuperClosure
 *
 * SuperClosure\Serializer Wrapper. Used to Serialize closures.
 *
 * @package Ds\Router\Serializer
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link    https://red-sqr.co.uk/Framework/Router/wikis/
 *
 * @see "jeremeamia/SuperClosure"
 */
class SuperClosure implements SerializerInterface
{
    /**
     * @var SuperClosureSerializer
     */
    public $serializer;

    /**
     * SuperClosure constructor.
     *
     * @param SuperClosureSerializer $serializer
     */
    public function __construct(SuperClosureSerializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function serialize($data)
    {
        $classRef = new \ReflectionClass($data);
        if ($classRef->getName() && $classRef->getName() !== 'Closure') {
            throw new RouteException('Class: ' . $classRef->getName() . ' can not be used as a handler. Must be a valid closure or string');
        }

        return \base64_encode($this->serializer->serialize($data));
    }

    /**
     * @inheritdoc
     * @throws \SuperClosure\Exception\ClosureUnserializationException
     */
    public function unserialize(string $string)
    {
        return $this->serializer->unserialize(\base64_decode($string));
    }
}
