<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router;

use Psr\Http\Message\ServerRequestInterface;
use Ds\Router\Interfaces\AdaptorInterface;
use Ds\Router\Interfaces\RouteCollectionInterface;
use Ds\Router\Interfaces\RouterInterface;

/**
 * Routing Class used to dispatch handlers from ServerRequestInterface.
 *
 * @package Ds\Router
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
class Router implements RouterInterface
{

    private $ignoreTrailingSlash = false;

    /**
     * @var AdaptorInterface $adaptor   Router Adaptor.
     */
    protected $adaptor;

    /**
     * @var RouteCollectionInterface $collection    Route Collection
     */
    protected $collection;

    /**
     * Router constructor.
     *
     * @param AdaptorInterface         $adaptor    Router Adaptor
     * @param RouteCollectionInterface $collection Route Collection
     * @param array $options Router Options
     */
    public function __construct(
        AdaptorInterface $adaptor,
        RouteCollectionInterface $collection,
        array $options = []
    ) {
        $this->adaptor = $adaptor;
        $this->collection = $collection;

        if (isset($options['ignoreTrailingSlash'])){
            $this->ignoreTrailingSlash = (bool)$options['ignoreTrailingSlash'];
        }

    }

    /**
     * @inheritdoc
     */
    public function getRouterResponse(ServerRequestInterface $request)
    {

        $requestUri = $request->getUri()->getPath();


        if ($this->ignoreTrailingSlash === true && $requestUri !== '/'){
            $requestUri = rtrim($requestUri,'/');
        }

        return $this->adaptor->match(
            $this->collection,
            $request->getMethod(),
            $requestUri
        );
    }

    /**
     * @inheritdoc
     */
    public function getRouterResponseFromPath(string $method, string $requestTarget)
    {
        return $this->adaptor->match(
            $this->collection,
            $method,
            $requestTarget
        );
    }

    /**
     * @inheritdoc
     */
    public function getAdaptor()
    {
        return $this->adaptor;
    }

    /**
     * @inheritdoc
     */
    public function withAdaptor(AdaptorInterface $adaptor)
    {
        $new = clone $this;
        $new->adaptor = $adaptor;
        return $new;
    }

    /**
     * @inheritdoc
     * @throws \Rs\Router\Exceptions\AdaptorException
     */
    public function getCollection()
    {
        if ($this->adaptor->isCached()) {
            return $this->adaptor->getCachedRoutes();
        }
        return $this->collection;
    }

    /**
     * @inheritdoc
     * @throws \Rs\Router\Exceptions\RouteException
     */
    public function mergeCollection(RouteCollectionInterface $collection)
    {
        return $this->withCollection(
            $this->collection->mergeCollection($collection)
        );
    }

    /**
     * @inheritdoc
     */
    public function withCollection(RouteCollectionInterface $collection)
    {
        $new = clone $this;
        $new->collection = $collection;
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function isCached($signature = null)
    {
        return $this->adaptor->isCached($signature);
    }

    /**
     * @param bool $ignore
     */
    public function disableTrailingSlash($ignore = true){
        $this->ignoreTrailingSlash = $ignore;
    }
}
