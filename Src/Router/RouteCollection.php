<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router;

use Ds\Router\Exceptions\RouteException;
use Ds\Router\Exceptions\UniqueRouteException;
use Ds\Router\Interfaces\RouteCollectionInterface;
use Ds\Router\Interfaces\RouteInterface;

/**
 * Class RouteCollection
 *
 * @package Ds\Router
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @see FileLoader
 * @see YamlLoader
 * @see RouteInterface
 * @see RouterInterface
 */
class RouteCollection implements RouteCollectionInterface
{
    /**
     *
     * Controller::Method delimiter.
     *
     * @var string
     */
    protected $delimiter = '::';

    /**
     * RouteInterfaces
     *
     * @var array.
     */
    protected $collection = [];
    /**
     * Route group context.
     *
     * @var array
     */
    protected $group = [];
    /**
     * Route group names.
     *
     * @var array
     */
    protected $groupNames = [];
    /**
     * Namespaces for Controller::Method handler.
     *
     * @var array
     */
    protected $namespace = [];
    /**
     * Global Collection route names.
     *
     * @var array
     */
    protected $globalRouteNames = [];
    /**
     * Is grouped callback flag.
     *
     * @var bool
     */
    private $_isGrouped = false;

    /**
     * Add Namespace to Route Collection.
     *
     * @param string $namespace Controller Namespace.
     */
    public function addNamespace(string $namespace)
    {
        $formattedNamespace = '\\'.ltrim($namespace,'\\');
        $this->namespace[] = $formattedNamespace;
    }

    /**
     * @param array $names
     */
    public function addGlobalNames(array $names)
    {
        foreach ($names as $name) {
            $this->globalRouteNames[$name] = $name;
        }
    }

    /**
     * @inheritdoc
     */
    public function group($path = '', callable $routeCallback, array $names = [])
    {
        $this->group[] = $path;
        $this->groupNames[] = $names;
        $this->_isGrouped = true;
        $routeCallback();
        $this->_isGrouped = false;
        \array_pop($this->group);
        \array_pop($this->groupNames);
    }

    /**
     * @inheritdoc
     *
     * @throws \Rs\Router\Exceptions\UniqueRouteException
     */
    public function addRoute($method, $pattern, $handler, $name = [])
    {
        $routePattern = $this->formatRoutePattern($pattern);
        $this->_isUnique($method, $routePattern);
        $routeNames = $this->_getRouteGlobals($this->_mergeGroupNames($name));

        if (\is_string($handler)) {
            $handler = $this->_findHandlerNamespace($handler);
        }

        foreach ((array)$method as $httpMethod) {
            $this->collection[] = new Route(
                $httpMethod,
                $routePattern,
                $handler,
                $routeNames
            );
        }
        return $routePattern;
    }

    /**
     * Add Namespace to handler.
     *
     * @param mixed $handler
     *
     * @return mixed
     */
    private function _findHandlerNamespace($handler)
    {
        $handler = \str_replace("/", "\\", $handler);

        if (\is_string($handler)) {
            $p = \explode($this->delimiter, $handler);
            if (isset($p[1])) {
                foreach ($this->namespace as $ns) {
                    if (\class_exists($ns . '\\' . $p[0])) {
                        $handler = $ns . '\\' . $p[0] . $this->delimiter . $p[1];
                    }
                }
            }
        }
        return $handler;
    }

    /**
     * Check that the Route is unique before adding it (again).
     *
     * @param  $method
     * @param  $pattern
     * @return bool
     * @throws UniqueRouteException
     */
    private function _isUnique($method, $pattern)
    {
        /**
         * @var Route $route
         */
        foreach ($this->collection as $route) {
            if ($route->getMethod() === $method && $route->getPattern() === $pattern) {
                throw new UniqueRouteException('Route has already been added.');
            }
        }
        return true;
    }

    /**
     * Get Collection global route names.
     *
     * @param array $names Routes names to merge with globals.
     *
     * @return array
     */
    private function _getRouteGlobals(array $names)
    {
        return \array_unique(
            \array_merge(
                \array_values($this->globalRouteNames), $names
            ), SORT_REGULAR
        );
    }

    /**
     * Merge Route Names with group.
     *
     * @param array $names Route names to be added to group.
     *
     * @return array
     */
    private function _mergeGroupNames(array $names)
    {
        $finalStack = [];

        if (\count($this->groupNames) !== 0) {
            $finalStack = \array_merge(...$this->groupNames);
        }

        return \array_unique(\array_merge($names, $finalStack));
    }

    /**
     * Format Route Pattern and return with grouped directory is necessary.
     *
     * @param  $pattern
     * @return string
     */
    public function formatRoutePattern($pattern)
    {
        $routePattern = '/' . \ltrim($pattern, '/');

        if ($this->_isGrouped === false) {
            return $routePattern;
        }

        $route = \implode('', $this->group) . $routePattern;

        if (substr($route, -1) === '/' && strlen($route) > 1){
            $route = rtrim($route,'/');
        }

        return $route;

    }

    /**
     * @inheritdoc
     */
    public function mergeCollection(RouteCollectionInterface $collection, bool $throw = true)
    {
        $new = clone $this;
        foreach ($collection as $addRoute) {
            try {
                $new->_isUnique(
                    $addRoute->getMethod(),
                    $this->formatRoutePattern(
                        $addRoute->getPattern()
                    )
                );
                $new->collection[] = $addRoute;
            } catch (\Exception $e) {
                if ($throw) {
                    throw new RouteException($e->getMessage());
                }
                continue;
            }
        }
        return $new;
    }

    /**
     * @inheritdoc
     */
    public function getRoutes()
    {
        return $this->collection;
    }

    /**
     * Returns current route.
     *
     * @return RouteInterface
     */
    public function current()
    {
        return \current($this->collection);
    }

    /**
     * Returns next route.
     *
     * @return RouteInterface
     */
    public function next()
    {
        return \next($this->collection);
    }

    /**
     * Returns current route index.
     *
     * @return mixed
     */
    public function key()
    {
        return \key($this->collection);
    }

    /**
     * If last route not reached.
     *
     * @return bool
     */
    public function valid()
    {
        $key = \key($this->collection);
        return ($key !== null && $key !== false);
    }

    /**
     * Return to and return first route.
     *
     * @return RouteInterface
     */
    public function rewind()
    {
        return \reset($this->collection);
    }

    /**
     * Get total routes in collection.
     *
     * @return int
     */
    public function count()
    {
        return \count($this->collection);
    }
}
