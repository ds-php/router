<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Interfaces;

use Ds\Router\Exceptions\RouterException;

/**
 * Interface LoaderInterface
 *
 * @package Ds\Router\Interfaces
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
interface LoaderInterface
{
    /**
     * Load Route File.
     *
     * @param string $file Route filename.
     * @return RouteCollectionInterface
     * @throws RouterException
     */
    public function loadFile(string $file);

    /**
     * Load multiple route files.
     *
     * @param array $files
     * @return RouterInterface
     * @throws RouterException
     */
    public function loadFiles(array $files);
}
