<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Interfaces;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Router Interface
 *
 * Adaptors are used to connect to components and return RouterResponses to the Router.
 *
 * @package Ds\Router\Interfaces
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
interface RouterInterface
{
    /**
     * Router constructor.
     * @param AdaptorInterface $adaptor
     * @param RouteCollectionInterface $collection
     */
    public function __construct(AdaptorInterface $adaptor, RouteCollectionInterface $collection);

    /**
     * Get router response from request.
     *
     * @param ServerRequestInterface $request
     * @return RouterResponseInterface
     */
    public function getRouterResponse(ServerRequestInterface $request);

    /**
     * @param string $method
     * @param string $path
     * @return RouterResponseInterface
     */
    public function getRouterResponseFromPath(string $method, string $path);

    /**
     * Returns Adaptor
     *
     * @return AdaptorInterface
     */
    public function getAdaptor();

    /**
     * With added Adaptor.
     *
     * @param AdaptorInterface $adaptor
     * @return RouterInterface
     */
    public function withAdaptor(AdaptorInterface $adaptor);

    /**
     * Returns Route Collection.
     *
     * @return RouteCollectionInterface
     */
    public function getCollection();

    /**
     * With added Route Collection.
     *
     * @param RouteCollectionInterface $collection
     * @return RouterInterface
     */
    public function withCollection(RouteCollectionInterface $collection);

    /**
     * Merge Route Collections.
     *
     * @param RouteCollectionInterface $collection
     * @return RouterInterface
     */
    public function mergeCollection(RouteCollectionInterface $collection);

    /**
     * Check if Routes have already been cached.
     * @param string|null $signature
     * @return bool
     */
    public function isCached($signature = null);

    /**
     * Ignore trailing slash from routes.
     * @param bool $ignore
     */
    public function disableTrailingSlash($ignore);
}
