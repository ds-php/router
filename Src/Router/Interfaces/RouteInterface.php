<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Interfaces;

/**
 * Interface RouteInterface
 *
 * @package Ds\Router\Interfaces
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @see RouteCollectionInterface
 */
interface RouteInterface
{
    /**
     * With route http method.
     *
     * @param string $method
     * @return RouteInterface
     */
    public function withMethod($method);

    /**
     * @param string $pattern
     * @return RouteInterface
     */
    public function withPattern($pattern);

    /**
     * @param string|\Closure $handler
     * @return RouteInterface
     */
    public function withHandler($handler);

    /**
     * @param array $name
     * @return RouteInterface
     */
    public function withNames(array $name);

    /**
     * Return Route Http Method
     *
     * @return string
     */
    public function getMethod();

    /**
     * Return route pattern
     *
     * @return string
     */
    public function getPattern();

    /**
     * Return route handler
     *
     * @return string|\Closure
     */
    public function getHandler();

    /**
     * Return route names
     *
     * @return array
     */
    public function getNames();

    /**
     * Return handler type
     * @return string
     */
    public function getHandlerType();
}
