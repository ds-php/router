<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Interfaces;

/**
 * Interface SerializerInterface
 *
 * Serializer Interface is used to serialize and unserialize Route closure handlers.
 *
 * @package Ds\Router\Interfaces
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
interface SerializerInterface
{
    /**
     * @param \Closure $data
     * @return string
     */
    public function serialize($data);

    /**
     * @param string $string
     * @return \Closure
     */
    public function unserialize(string $string);
}
