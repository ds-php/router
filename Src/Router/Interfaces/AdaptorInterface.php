<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Interfaces;

use Ds\Router\Exceptions\AdaptorException;

/**
 * Routing Component Adaptor Interface
 *
 * Adaptors connect to routing components and return RouterResponses to the Router.
 *
 * @package Ds\Router\Interfaces
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */
interface AdaptorInterface
{
    /**
     * Match Request Method and Request Target from route returning a RouterResponse.
     *
     * @param RouteCollectionInterface $routes        Route Collection
     * @param string                   $method        Http Request Method
     * @param string                   $requestTarget Request Target
     *
     * @return RouterResponseInterface
     */
    public function match(RouteCollectionInterface $routes, $method, $requestTarget);

    /**
     * Get Adaptor Options.
     *
     * @return array
     */
    public function getOptions();

    /**
     * Get Adaptor option but it's index.
     *
     * @param string $key   Option Key.
     * @return mixed
     */
    public function getOption($key);

    /**
     * Creates a new instance of the Adaptor with given options.
     *
     * @param  array $options   Adaptor options.
     * @return AdaptorInterface
     */
    public function withOptions(array $options = []);

    /**
     * Check if cache is enabled and that there is a valid cache file.
     *
     * @return boolean
     */
    public function isCached();

    /**
     * Get RouteCollection from cache routes.
     *
     * @param string $context Route Context
     *
     * @return RouteCollectionInterface
     * @throws AdaptorException
     */
    public function getCachedRoutes($context = '');
}
