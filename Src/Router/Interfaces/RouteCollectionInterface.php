<?php
/**
 * This file is part of the DS Framework.
 *
 * (c) Dan Smith <dan--smith@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Ds\Router\Interfaces;

use Ds\Router\Exceptions\RouteException;

/**
 * Interface RouteCollectionInterface
 *
 * @package Ds\Router\Interfaces
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 *
 * @see RouteInterface
 */
interface RouteCollectionInterface extends \Iterator
{
    /**
     * Add group of routes
     *
     * @param string $path
     * @param callable $routeCallback
     * @param array $names Group Route name
     * @return void
     */
    public function group($path = '', callable $routeCallback, array $names = []);

    /**
     * Add Route to Application.
     *
     * @param string|array $method
     * @param string $pattern
     * @param string|\Closure $handler
     * @param array $name
     * @return mixed
     */
    public function addRoute($method, $pattern, $handler, $name);

    /**
     * Add routes to collection.
     * @param RouteCollectionInterface $collection
     * @param bool $throw
     * @return RouteCollectionInterface
     * @throws RouteException
     */
    public function mergeCollection(RouteCollectionInterface $collection, bool $throw = true);

    /**
     * Get Routes.
     * @return array
     */
    public function getRoutes();
}
