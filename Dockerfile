FROM php:7.4-cli

ADD ./ /var/www/Src
WORKDIR /var/www/Src

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
  && php composer-setup.php \
  && php -r "unlink('composer-setup.php');"

RUN php composer.phar install && vendor/bin/phpunit --configuration phpunit.xml --coverage-text