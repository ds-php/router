<?php

/**
 * ROUTES
 * PHP ROUTE FILE
 *
 * Load Files via
 * App:loadFile(string $filename, array $accessibleVars)
 * App:loadFiles(array $filesnames, array $accessibleVars)
 */

/**
 * Route Global Vars
 * Variables made accessible from App::loadFile/s: $app, $bar @see example.php
 * These values will update regardless of cache.
 */
$container = 'container';

/**
 * Locale Route Vars.
 * Only available on this page.
 * If route cache is enabled then these values will be hardcoded to the route.
 * e.g. as long as the cache is valid $something will remain __FOO__ even if in the route.
 */
$something = '__FOO__';
$collection = new \Ds\Router\RouteCollection();

$collection->addRoute('GET', '/path-2', 'myClass::myMethod', ['name']);
$collection->addRoute('GET', '/alt-path', 'another-handler', ['name']);

return $collection;
